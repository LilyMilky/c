#include <stdio.h>
#include <stdlib.h>

int sommeTableau(int tableau[], int tailleTableau)
{
    int Somme = 0;
    int i;

    for(i = 0; i < tailleTableau; i++)
    {
      Somme = Somme + tableau[i];
    }

    return Somme / tailleTableau;
}

int main()
{
    int tableau[3] = {10,20,30};
    int Somme = 0;

    Somme = sommeTableau(tableau,3);
    printf("La moyenne des valeurs du tableau : %d\n", Somme);
    return 0;
}
