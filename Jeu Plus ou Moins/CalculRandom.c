#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int CalculRandom(int MIN, int MAX)
{
    srand(time(NULL));
    return (rand() % (MAX - MIN + 1)) + MIN;
}
