#include <stdio.h>
#include <stdlib.h>
#include "Fonction.h"
#include "dico.h"

int main()
{
    char motSecret[100] = {0};
    int *lettreTrouvee = NULL;
    char caractere;
    int i;
    int vie = 10;
    int trouve;

    if (!piocherMot(motSecret))
        exit(0);

    int tailleMotSecret = strlen(motSecret);

    lettreTrouvee = malloc(tailleMotSecret * sizeof(int)); // On alloue dynamiquement le tableau lettreTrouvee
    if (lettreTrouvee == NULL)
        exit(0);

    for (i = 0 ; i < tailleMotSecret ; i++)
        lettreTrouvee[i] = 0;

    printf("Bienvenue sur le pendu!\n");

    do
    {
        trouve = 0;
        printf ("Mot Secret : ");
        for (i = 0 ; i < tailleMotSecret ; i++)
        {
            if (lettreTrouvee[i]) // Si on a trouvé la lettre n° i
                printf("%c", motSecret[i]); // On l'affiche
            else
                printf("*"); // Sinon, on affiche une étoile pour les lettres non trouvées
        }

        printf("\nVeuillez saisir une lettre: ");
        caractere = lireCaractere();
        printf("%c\n", caractere);

        for(i = 0; i < sizeof(motSecret); i++)
        {
            if(motSecret[i] == caractere)
            {
                lettreTrouvee[i] = 1;
                trouve = 1;
            }
        }

        if(trouve == 0)
        {
            vie--;
            printf("Faux! Il vous reste %d coups\n", vie);
        }

    }while((vie > 0) && (!gagne(lettreTrouvee, tailleMotSecret)));

    if(vie == 0)
        printf("Vous avez perdu! le mot était %s", motSecret);
    else
        printf(" Felicitation, vous avez gagne!");

    return 0;
}
