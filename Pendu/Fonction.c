#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#include "fonction.h"

char lireCaractere()
{
    char caractere = 0;

    caractere = getchar(); // On lit le premier caractère
    caractere = toupper(caractere); // On met la lettre en majuscule si elle ne l'est pas déjà

    // On lit les autres caractères mémorisés un à un jusqu'au \n (pour les effacer)
    while (getchar() != '\n') ;

    return caractere; // On retourne le premier caractère qu'on a lu
}

int gagne(int lettreTrouvee[], int tailleMotSecret)
{
    int i;
    int joueurGagne = 1;

    for(i = 0; i < tailleMotSecret; i++)
    {
        if(lettreTrouvee[i] == 0)
            joueurGagne = 0;
    }

    return joueurGagne;
}
