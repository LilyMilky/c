#include <stdio.h>
#include <stdlib.h>

#define TAILLE_MAX 1000

int fseek(FILE* pointeurSurFichier, long deplacement, int origine);

int main()
{
    FILE* fichier = NULL;
    char chaine[TAILLE_MAX] = "";

    fichier = fopen("Test/FichierTest.txt", "r+");

    if (fichier != NULL)
    {
        // On peut lire et écrire dans le fichier

        while (fgets(chaine, TAILLE_MAX, fichier) != NULL) // On lit le fichier tant qu'on ne reçoit pas d'erreur (NULL)
        {
            printf("%s", chaine); // On affiche la chaîne qu'on vient de lire
        }

        fputs("Salut ", fichier);

        fclose(fichier);
    }
    else
    {
        // On affiche un message d'erreur si on veut
        printf("Impossible d'ouvrir le fichier test.txt");
    }

    return 0;
}
