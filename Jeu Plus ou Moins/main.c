#include <stdio.h>
#include <stdlib.h>
#include "CalculRandom.h"

int main()
{
    const int MIN = 0;
    const int MAX = 100;
    int *pointeurMIN = &MIN; //Test Pointeur
    int nombreMystere = CalculRandom(MIN,MAX);
    int ValeurSaisie;
    int coup = 0;

    printf("Bienvenue sur ce jeu de + ou -\n");
    printf("La valeur Min est : %d\n", *pointeurMIN);

    do
    {
        printf("Veuillez saisir un nombre: ");
        scanf("%d", &ValeurSaisie);
        coup++;

        if(nombreMystere == ValeurSaisie)
        {
            printf("Bonne reponse, vous avez gagnez!\n");
            printf("Vous avez trouvez la reponse en %d coup! \n", coup);
        }
        else if(nombreMystere > ValeurSaisie)
        {
            printf("C'est + !\n");
        }
        else
        {
            printf("C'est - !\n");
        }
    }while(nombreMystere != ValeurSaisie);

    return 0;
}


