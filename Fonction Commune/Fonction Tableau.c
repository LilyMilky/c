void affiche(int *tableau, int tailleTableau)
{
    int i;

    for (i = 0 ; i < tailleTableau ; i++)
    {
        printf("%d\n", tableau[i]);
    }
}
/////////////////////////////////////////////////////////////////////////////////////////////
int sommeTableau(int tableau[], int tailleTableau)
{
    int Somme = 0;
    int i;

    for(i = 0; i < tailleTableau; i++)
    {
      Somme = Somme + tableau[i];
    }

    return Somme;
}
////////////////////////////////////////////////////////////////////////////////////////////
int moyenneTableau(int tableau[], int tailleTableau)
{
    int Somme = 0;
    int i;

    for(i = 0; i < tailleTableau; i++)
    {
      Somme = Somme + tableau[i];
    }

    return Somme / tailleTableau;
}
/////////////////////////////////////////////////////////////////////////////////////////////
